**To integrate your app with YouGiver you need:**

1. Adding an icon with a gift to the interface ([Link](https://drive.google.com/drive/folders/1nBWYIg5fY13gFmbhLavH9uzr7wxcDNxN));

2. To select the set of gift giving options available for integration into your app. Depending on the data being transmitted all available options are listed below:

  **Option №1**  
  *The following is required:*
  1. the identifier of the Giver;  
  2. the mechanism for sending notifications to the Giver (YouGiver can send notifications by API or send notifications to the Giver's email, if any will be provided to YouGiver).

  **The Recipient** for this integration option can be from your application or any external system.

  *For implementation, you will need to provide the following to YouGiver:*    
   1. the identifier of your application;  
   2. the identifier of the Giver in your system;  
   3. the email of the Giver (optional, for sending notifications).

  **Option №2**  
  *The following is required:*   
  1. the identifier of the Giver;  
  2. the identifier of the Recepient;  
  3. the mechanism for sending notifications to the Giver/Receiver (YouGiver can send notifications by API or send notifications to the Giver's/Receiver's email, if any will be provided to YouGiver).

  **The Recipient** for this integration option must be the user of your application.
  
  *For implementation, you will need to provide the following to YouGiver:*  
	1. the identifier of the your application;  
	2. the identifier of the Giver in your system;  
	3. the identifier of the Receiver in your system;  
	4. the email of the Giver/Receiver (optional, for sending notifications).
   
  **Option №3**  
  *The following is required:*  
	1. the identifier of the Giver;  
	2. the identifier of the Recepient;  
	3. the Recipient's country and city;  
	4. the mechanism for sending notifications to the Giver/Receiver (YouGiver can send notifications by API or send notifications to the Giver's/Receiver's email, if any will be provided to YouGiver).

  **The Recipient** for this integration option must be the user of your application.
 
  *For implementation, you will need to provide the following to YouGiver:*  
	1. the identifier of your application;  
	2. the identifier of the Giver in your system;  
	3. the identifier of the Receiver in your system;  
	4. the email of the Giver/Receiver (optional, for sending notifications).
	
**Testing**

1. In the your personal account, create/set up WebHooks with the necessary event notifications for the interaction options 
you are planning on integrating into your app.

2. Implement the initialization of the chosen Options (using the **developer_number**) in your app according to the documentation concerning YouGiver variants (see below).

3. Test the correct work and **connected notifications**.

4. Launch integration in the production mode.