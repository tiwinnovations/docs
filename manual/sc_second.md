## **2 Option description**

The gift giving process under the Option 2 provides for:

1. the user who will act as the Giver and the user who will act as the Presentee are users of your application;
2. on your application side there is **NO** information about:
	- the country of residence of users who act as the Giver and the Presentee;
	- the city of residence of users who act as the Giver and the Presentee.
3. after the initialization of the 2 Scenario, the Giver will be able to continue the gift giving process using the branch of the:
 - 2a Option (Giver knows Presentee’s  country and the city of residence);
 - or 2b Option (Giver does not know anything about location of the Presentee).

**The 2a Option branch** stipulates that the user who will act as the Giver knows the country and city of residence of the user who will act as the Presentee.

**The 2b Option branch** stipulates that the user who will act as the Giver does **NOT** know the Presentee’s country and city of residence. In this case, the link to the form is sent to Presentee to fill out the data for the delivery of the gift.


### **2 Option Initialization**

1. Send the POST request to `https://service.yougiver.me/api/v1/gift_requests` with the possible parameters:

<!--table-->

| Paremeter                                | Description                                  |
|---------------------------------------  |------------------------------------------ |
| gift_request[developer_number] **\***   | Your developer_number in YouGiver service   |
| gift_request[giver_id] **\***           | The Giver ID in your service    |
| gift_request[giver_name]                | Name of the Giver                              |
| gift_request[giver_email]               | Email of the Giver                            |
| gift_request[giver_phone]               | Giver's phone number                          |
| gift_request[giver_nickname]            | Nickname of the Giver                          |
| gift_request[giver_gender]              | Gender of the Giver                              |
| gift_request[recipient_id] **\***       | The Presentee ID in your service  |
| gift_request[recipient_name]            | Presentee's name                            |
| gift_request[recipient_email]           | Presentee's email                          |
| gift_request[recipient_address]         | Presentee's address                          |
| gift_request[recipient_gender]          | Gender of the Presentee                            |
| gift_request[recipient_nickname]        | Presentee's Nickname                        |

<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Parameter | Description                                      |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow. |

<!--endtable-->

**Parameters description**

**All parameters are simple strings.**

**Supported event notifications in 2a Option**

1. **Event:** Payment for the gift by the Giver → **Notification #1:** "Presentee’s notification of a new gift."
2. **Event:** Filling out contact information by the Presentee → **Notification #2:** "The Giver's notification of the gift confirmation. Filling out the contact information by the Presentee."
3. **Event:** Change of order status to "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
4. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
5. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **Notification #5:** "Notifying the Giver that the Presentee did not confirm (ignored) accepting of the gift."
6. **Event:** Payment by the gift Giver. In case the Presentee has never previously confirmed the receipt of the gift. → **Notification #7:** "Notifying the Presentee. "Why did I get this link."

**Supported event notifications in 2b Option**

1. **Event:** Payment for the gift by the Giver → **Notification #1:** "Presentee’s notification of a new gift."
2. **Event:** Change of order status to "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
3. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
4. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **Notification #5:** "Notifying the Giver that the Presentee did not confirm (ignored) accepting of the gift."
5. **Event:** Presentee has provided the delivery information → **Notification #6:** "Notifying the Giver on Presentee's acceptnce of the gift.
6. **Event:** Payment by the gift Giver. In case the Presentee has never previously confirmed the receipt of the gift. → **Notification #7:** "Notifying the Presentee. "Why did I get this link."

**Testing**

1. In the your personal account, create/set up WebHooks with the necessary event notifications for the interaction options you are planning on integrating into your app.

2. Implement the initialization of the chosen Option (using the developer_number) in your app according to the documentation concerning YouGiver variants (see below).

3. Test the correct work and connected notifications.

4. Launch integration in the production mode.