## **1 Option description**

The gift giving process under 1 option provides for:

1. the user who will act as the Giver is the user of your app;
2. the user who will act as the Presentee can be from your application or any external system;
3. on your application side there is **NO** information about:
	- the country of residence of user who act as the Presentee;
	- the city of residence of users who act as the Presentee;
4. after the initialization of the 1 Option, the Giver will be able to continue the gift giving process using the branch of the:
 	- 1a Option (Giver knows full delivery information);
 	- or 1b Option (Giver does not know anything about location of the Presentee);
 	- or 1c Option (Giver knows Presentee’s  country and the city of residence).
  
### **1 Option initialization**

1. Send the POST request to `https://service.yougiver.me/api/v1/gift_requests` with the possible parameters:

<!--table-->

| Parameter                               | Description                                  |
|---------------------------------------  |------------------------------------------ |
| gift_request[developer_number] **\***   | Your developer_number in Yougiver service   |
| gift_request[giver_id] **\***           | Giver's ID in your service    |
| gift_request[giver_name]                | Giver's Name                              |
| gift_request[giver_email]               | Giver's Email                            |
| gift_request[giver_phone]               | Giver's Phone number                          |
| gift_request[giver_nickname]            | Giver's Nickname                          |
| gift_request[giver_gender]              | Giver's Gender                              |
<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Parameter | Description                                      |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

----

**1a Option description (Giver knows full delivery information)**

The gift giving process under 1a Option provides for:

1. the user who will act as the Giver is the user of your app;
2. the user who will act as the Presentee can be from your application or any external system;;
3. on your application side there is **NO** information about:
	- the country of residence of users who act as the Presentee;
	- the city of residence of users who act as the Presentee;
4. after the initialization of the 1a Option, the Giver will be able to specify all the necessary information for delivery of the gift to the Presentee.

**Supported event notifications in 1a Option**

1. **Event:** Change of order status to "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."

2. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."

----

**1b Option description (Giver does not know anything about location of the Presentee)**

The gift giving process under the Option 1b provides for:

1. the user who will act as the Giver is the user of the your app;
2. the user who will act as the Presentee is **NOT** a user of the your app;
3. on the your application side there is **NO** information about:
	- the country of residence of users who act as the Presentee;
	- the city of residence of users who act as the Presentee;
4. sending the link to the Presentee to the form to fill out the gift delivery information to any app installed on the device of the Giver (the Giver chooses it).

**Supported event notifications in 1b Option**

1. **Event:** Change of order status to - "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
2. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
3. **Event:** Provision of data for delivery by the Recipient → **Notification #6:** "Notifying the Giver that the Presentee has agreed to receive the gift."
4. **Event:** 3b Scenario initialization → **Notification #8**: "Notification for the Giver with a link to be shared with Presentee".

----

**1c Option description (Giver knows Presentee’s  country and the city of residence)**

The gift giving process under the Option 1c provides for:

1. the user who will act as the Giver is the user of the your app;
2. the user who will act as the Presentee is **NOT** a user of the your app;
3. on your application side there is **NO** information about:
	- the country of residence of users who act as the Presentee;
	- the city of residence of users who act as the Presentee;
4. Giver knows the country and city of residence of the user who will act as the Presentee;
5. sending the link to the Presentee to the form to fill out the gift delivery information to any app installed on the device of the Giver (the Giver chooses it).

**Supported event notifications in 1c Option**

1. **Event:** Filling out contact information by the Presentee → **Notification #2:** "The Giver's notification of the gift confirmation. Filling out the contact information by the Presentee."
2. **Event:** Change of order status to - "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
3. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
4. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **Notification #5:** "Notifying the Giver that the Presentee did not confirm (ignored) accepting of the gift."
5. **Event:** Payment for the gift by the Giver → **Notification #8**: "Notification for the Giver with a link to be shared with Presentee".

**Testing**

1. In the your personal account, create/set up WebHooks with the necessary event notifications for the interaction options you are planning on integrating into your app.

2. Implement the initialization of the chosen Options (using the developer_number) in your app according to the documentation concerning YouGiver variants (see below).

3. Test the correct work and connected notifications.

4. Launch integration in the production mode.