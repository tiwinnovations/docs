### **Description**

These APIs can be used to better integration of developer with the YouGiver service.

Can be used to obtain lists of countries and cities in which YouGiver services operates.

### **API to get a list of available countries in YouGiver service**

Request of a country code
  `https://service.yougiver.me/api/v1/locations/all_countries`

  Parameters:
  - `locale` - locale code (must be available in the system, optional parameter)

  Examples:
    `https://service.yougiver.me/api/v1/locations/all_countries?locale=ru`
    `https://service.yougiver.me/api/v1/locations/all_countries`

  Reply format:

    id: Country code
    name: Name
    display_name: Official name (may be left out)

### **API to get a list of available cities in a specific country in the YouGiver service**

Cities in the specified country
  `https://service.yougiver.me/api/v1/locations/all_cities`

  Parameters:
  - `cc` - Country code
  - `locale` - Locale code (should be available in the system, optional parameter)

  Examples:
    `https://service.yougiver.me/api/v1/locations/all_cities?cc=dk`
    `https://service.yougiver.me/api/v1/locations/all_cities?cc=dk&locale=en`

  Reply format:

    name: Name
    display_name: Full location address (may be left out)
